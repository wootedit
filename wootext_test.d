// coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
// Understanding is not required. Only obedience.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
module woot_test is aliced;

import woot2;

import iv.prng.pcg32;
import iv.vfs.io;


// ////////////////////////////////////////////////////////////////////////// //
__gshared PCG32 prng;


int randomInt (int max) {
  if (max < 1) return 0;
  auto res = cast(int)(prng.front%max);
  prng.popFront();
  return res;
}


char randomChar () => cast(char)(97+randomInt(26));


string randomString (int maxLen=0) {
  maxLen |= 16;
  string s;
  int sLen = randomInt(maxLen+1);
  foreach (immutable _; 0..sLen) s ~= randomChar();
  return s;
}


WStrOp[] randomOperation (WString ws) {
  auto str = ws.toString();
  // insert?
  if (str.length == 0 || randomInt(2) == 0) return ws.genInsOp(randomInt(cast(int)str.length), randomString());
  // delete
  int len = randomInt(cast(int)str.length)+1;
  int ofs = (len == str.length ? 0 : randomInt(cast(int)str.length-len));
  return ws.genDelOp(ofs, len);
}


void applyInRandomOrder (WString ws, WStrOp[] ops) {
  while (ops.length > 0) {
    auto pos = randomInt(cast(int)ops.length);
    ws.applyRemote(ops[pos]);
    if (ops.length == 1) break;
    ops[pos] = ops[$-1];
    ops.length -= 1;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void testAppend () {
  auto o = new WString(1);
    o.genInsOp(0, "lorem");
    o.genInsOp(5, " ipsum");
  assert(o.toString == "lorem ipsum");
  //assert(o.ops.length == 3);
  //writeln("o=", o.toString);
  /+
  auto reso = new TextOperation();
  reso.ops ~= TextOp("lorem ipsum");
  reso.ops ~= TextOp(8);
  reso.ops ~= TextOp(-8);
  //writeln("x=", reso.toString);
  //assert(o == TextOperation({"lorem ipsum", 8, -8}))
  assert(o == reso);
  +/
}


void testApply () {
  string doc = "Lorem ipsum";
  auto op = new WString(1);
    op.genInsOp(0, doc);
    op.genDelOp(0);
    op.genInsOp(0, 'l');
    op.genDelOp(5, 4);
    op.genInsOp(7, "s");
  //writeln("op=<", op.toString, ">");
  //writeln("op=", op.chars2str);
  assert(op.toString == "loressum");
}


void testRandomOps () {
  auto s0 = new WString(1);
  auto op0 = s0.genInsOp(0, randomString(42));
  auto s1 = new WString(2);
  //writeln(op0.length);
  //writeln(op0);
  s1.applyRemote(op0);
  //writeln(s0.toString);
  //writeln(s1.toString);
  //assert(s0.toString == s1.toString);
  //writeln(s0.toString);
  WStrOp[] opcoll;
  foreach (immutable _; 0..42) {
    auto rop = randomOperation(s0);
    //writeln(_, ": rop=", rop, "; after=", s0);
    opcoll ~= rop;
  }
  applyInRandomOrder(s1, opcoll);
  assert(s0.toString == s1.toString);
  //writeln("s0=", s0.mChars);
  //writeln("s1=", s1.mChars);
  //assert(s0.mChars.length == s1.mChars.length);
  //writeln(s0.mChars[0], " -- ", s1.mChars[0]);
  /*
  writeln(s0.mChars[0].id, " -- ", s1.mChars[0].id);
  writeln(s0.mChars[0].id.mSiteNumber, ", ", s0.mChars[0].id.mOpNumber);
  writeln(s1.mChars[0].id.mSiteNumber, ", ", s1.mChars[0].id.mOpNumber);
  writeln(s0.mChars[0].id.mSiteNumber == s1.mChars[0].id.mSiteNumber, " : ", s0.mChars[0].id.mOpNumber == s1.mChars[0].id.mOpNumber);
  writeln(s0.mChars[0].id == s1.mChars[0].id);
  assert(s0.mChars[0].id == s1.mChars[0].id);
  */
  //assert(s0.mChars[0] == s1.mChars[0]);
  //writeln(s0.mChars);
  //writeln(s1.mChars);
  /+
  assert(s0.mChars.length == s1.mChars.length);
  assert(s0.mChars[0] == s1.mChars[0]);
  foreach (immutable idx; 0..s0.mChars.length) {
    if (s0.mChars[idx] != s1.mChars[idx]) {
      import std.format : format;
      assert(0, "oops: idx=%s; s0c:%s; s1c:%s".format(idx, s0.mChars[idx], s1.mChars[idx]));
    }
  }
  +/
  assert(s0 == s1);
}


void testRandomOps2 () {
  auto s0 = new WString(1);
  auto op0 = s0.genInsOp(0, randomString(42));
  auto s1 = new WString(2);
  //writeln(op0.length);
  //writeln(op0);
  s1.applyRemote(op0);
  //writeln(s0.toString);
  //writeln(s1.toString);
  //assert(s0.toString == s1.toString);
  //writeln(s0.toString);
  foreach (immutable _; 0..42) {
    auto rop = randomOperation(s0);
    //writeln(_, ": rop=", rop, "; after=", s0);
    applyInRandomOrder(s1, rop);
    assert(s0.toString == s1.toString);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void main () {
  // create first string
  auto s0 = new WString(1);
  // create second string
  auto s1 = new WString(2);
  // modify first string, fix second
  s1.applyRemote(s0.genInsOp(0, 'a'));
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  // modify first string, fix second
  s1.applyRemote(s0.genInsOp(0, 'b'));
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  // modify first string, fix second
  s1.applyRemote(s0.genInsOp(1, 'c'));
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  auto op0 = s0.genDelOp(0);
  writeln("op0: ", op0.toString);
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  auto op1 = s0.genDelOp(1);
  writeln("op1: ", op1.toString);
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  s1.applyRemote(op1);
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  s1.applyRemote(op0);
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  s1.applyRemote(op1);
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  s1.applyRemote(op0);
  writeln("s0: <", s0.toString, ">; s1: <", s1.toString, ">");
  //
  testAppend();
  testApply();
  foreach (immutable _; 0..500) testRandomOps();
  foreach (immutable _; 0..500) testRandomOps2();
}
