// coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
// Understanding is not required. Only obedience.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//version = use_nanovg;

import std.uuid;

import arsd.color;
import arsd.simpledisplay;

import iv.cmdcon;
version(use_nanovg) {
  import iv.cmdcongl;
  import iv.nanovg;
}
import iv.unarray;
import iv.utfutil;

import iv.vfs.io;

import wootext;
import wootnet;



// ////////////////////////////////////////////////////////////////////////// //
enum WinWidth = 800;
enum WinHeight = 600;


// ////////////////////////////////////////////////////////////////////////// //
public class NetProcessEvent {}
__gshared NetProcessEvent evNetProcess;

shared static this () {
  evNetProcess = new NetProcessEvent();
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared ushort optPort = 16469;
__gshared string optKey = "alice";


// ////////////////////////////////////////////////////////////////////////// //
version(Windows) {
  enum ttfFile = "arial.ttf:noaa";
} else {
  enum ttfFile = "~/ttf/ms/arial.ttf:noaa";
}
__gshared string optEditorFont = ttfFile;
__gshared int optEditorFontSize = 14;
version(use_nanovg) {
  __gshared NVGContext nvg = null;
}


// ////////////////////////////////////////////////////////////////////////// //
class WootPad {
private:
  WString wtext;

private:
  WootNet mNet;
  int mTopChar;
  int mCurPos;
  char[] mRLine; // render line; temporary buffer, will be actively reused

private:
  // will stop on '\n' or when `cpos` reached
  void collectLine(T) (ref T rng, int cpos=-1) {
    char[4] uc = void;
    mRLine.unsafeArraySetLength(0);
    while (!rng.empty) {
      if (cpos >= 0 && rng.curpos == cpos) break;
      auto ch = rng.front;
      assert(ch.visible);
      if (ch.ch == '\n') break;
      auto len = utf8Encode(uc[], ch.ch);
      //static if (dump) writeln("pos=", rng.curpos, "; len=", len, "; ch=", cast(uint)ch.ch, "; cc=", ch);
      foreach (char ucc; uc[0..len]) mRLine.unsafeArrayAppend(ucc);
      rng.popFront();
    }
  }

private:
  void onInsert (int pos, in ref WChar ch) {
    if (pos <= mCurPos) ++mCurPos;
    if (pos < mTopChar) ++mTopChar;
  }

  void onDelete (int pos, in ref WChar ch) {
    if (pos >= mCurPos) mCurPos = wtext.fixVisible(pos);
    if (pos < mTopChar) mTopChar = wtext.fixVisible(mTopChar);
  }

public:
  this (WootNet anet) {
    mNet = anet;
    wtext = new WString(mNet.mycid);

    wtext.onInsert = &onInsert;
    wtext.onDelete = &onDelete;

    //mNet.queue(wtext.genInsOp(0, "Hello!"));
  }

  final @property WootNet net () { pragma(inline, true); return mNet; }
  final @property WString text () { pragma(inline, true); return wtext; }

  final void remote() (in auto ref WStrOp op) { wtext.applyRemote(op); }

  // user should call `ctx.beginFrame()` before calling this
  // returns `true` if cursor should be rendered
  version(use_nanovg) {
    bool render (NVGContext ctx, int* curx=null, int* cury0=null, int* cury1=null) {
      if (ctx is null) return false;

      bool res = false;
      int y = 0, lh;
      float[4] tbounds;
      auto rng = wtext.range(mTopChar);
      //writeln(rng.curpos);

      if (curx !is null) *curx = 0;
      if (cury0 !is null) *cury0 = 0;
      if (cury1 !is null) *cury1 = 0;

      ctx.textAlign(NVGTextAlign.H.Left, NVGTextAlign.V.Top);

      float asc, desc, lineh;
      ctx.textMetrics(&asc, &desc, &lineh);
      lh = cast(int)(lineh+0.9);

      if (mTopChar == 0 && rng.empty) {
        if (curx !is null) *curx = 0;
        if (cury0 !is null) *cury0 = y;
        if (cury1 !is null) *cury1 = y+lh;
        return true;
      }

      while (y < ctx.height && !rng.empty) {
        int lnstart = rng.curpos;
        collectLine(rng);
        if (!rng.empty) { assert(rng.front.ch == '\n'); rng.popFront(); }
        int lnend = rng.curpos;
        if (rng.empty) ++lnend;
        ctx.text(0, y, mRLine);
        // cursor
        if (curx !is null || cury0 !is null || cury1 !is null) {
          if (mCurPos >= lnstart && mCurPos < lnend) {
            res = true;
            if (curx !is null) {
              auto xrng = wtext.range(lnstart);
              collectLine(xrng, mCurPos);
              *curx = cast(int)(ctx.textBounds(0, y, mRLine, null)+0.5);
            }
            if (cury0 !is null) *cury0 = y;
            if (cury1 !is null) *cury1 = y+lh;
          }
        } else {
          res = res || (mCurPos >= lnstart && mCurPos < lnend);
        }
        // advance y
        y += lh;
        // cursor after last '\n'?
        if ((curx !is null || cury0 !is null || cury1 !is null) && rng.empty && mCurPos == wtext.length) {
          int vp = wtext.lastVisible();
          if (vp >= 0 && wtext[vp].ch == '\n') {
            res = true;
            if (curx !is null) *curx = 0;
            if (cury0 !is null) *cury0 = y;
            if (cury1 !is null) *cury1 = y+lh;
          }
        }
      }

      return res;
    }
  } else {
    bool render (ref ScreenPainter dwg, int* curx=null, int* cury0=null, int* cury1=null) {
      dwg.fillColor = Color.black;
      dwg.outlineColor = Color.black;
      dwg.drawRectangle(Point(0, 0), WinWidth, WinHeight);
      dwg.fillColor = Color.yellow;
      dwg.outlineColor = Color.yellow;

      int lh = dwg.fontHeight();
      bool res = false;
      int y = 0;
      auto rng = wtext.range(mTopChar);

      if (mTopChar == 0 && rng.empty) {
        if (curx !is null) *curx = 0;
        if (cury0 !is null) *cury0 = y;
        if (cury1 !is null) *cury1 = y+lh;
        return true;
      }

      while (y < WinHeight && !rng.empty) {
        int lnstart = rng.curpos;
        collectLine(rng);
        if (!rng.empty) { assert(rng.front.ch == '\n'); rng.popFront(); }
        int lnend = rng.curpos;
        if (rng.empty) ++lnend;
        //conwriteln(y, ": ", mRLine, " | ", lh);
        dwg.drawText(Point(0, y), mRLine);
        // cursor
        if (curx !is null || cury0 !is null || cury1 !is null) {
          if (mCurPos >= lnstart && mCurPos < lnend) {
            res = true;
            if (curx !is null) {
              auto xrng = wtext.range(lnstart);
              collectLine(xrng, mCurPos);
              auto sz = dwg.textSize(mRLine);
              *curx = sz.width;
            }
            if (cury0 !is null) *cury0 = y;
            if (cury1 !is null) *cury1 = y+lh;
          }
        } else {
          res = res || (mCurPos >= lnstart && mCurPos < lnend);
        }
        // advance y
        y += lh;
        // cursor after last '\n'?
        if ((curx !is null || cury0 !is null || cury1 !is null) && rng.empty && mCurPos == wtext.length) {
          int vp = wtext.lastVisible();
          if (vp >= 0 && wtext[vp].ch == '\n') {
            res = true;
            if (curx !is null) *curx = 0;
            if (cury0 !is null) *cury0 = y;
            if (cury1 !is null) *cury1 = y+lh;
          }
        }
      }

      return res;
    }
  }

  void onKeyEvent (KeyEvent evt) {
    if (!evt.pressed) return;
    if (evt == "Left") { mCurPos = wtext.prevVisible(mCurPos); return; }
    if (evt == "Right") { mCurPos = wtext.nextVisible(mCurPos); return; }
    if (evt == "Enter") { mNet.queue(wtext.genInsOp(mCurPos, '\n')); return; }
    if (evt == "Delete") { mNet.queue(wtext.genDelOp(mCurPos)); return; }
    if (evt == "Backspace") {
      auto pcur = wtext.prevVisible(mCurPos);
      if (pcur != mCurPos) mNet.queue(wtext.genDelOp(pcur));
      return;
    }
  }

  void onCharEvent (dchar ch) {
    if (ch < ' ' || ch == 127) return; // just in case
    mNet.queue(wtext.genInsOp(mCurPos, ch));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
__gshared WootPad wpad;
__gshared WootNet wnet;


// ////////////////////////////////////////////////////////////////////////// //
void main (string[] args) {
  version(use_nanovg) {
    glconShowKey = "M-Grave";
    setOpenGLContextVersion(3, 2); // up to GLSL 150
  }

  conRegVar!optEditorFont("ed_font_path", "path to TTF font");
  conRegVar!optEditorFontSize(6, 72, "ed_font_size", "editor font size");

  conRegVar!optPort(1, 65535, "net_port", "network port");
  conRegVar!optKey("net_key", "network key");

  conProcessQueue(); // load config
  conProcessArgs!true(args);

  wnet = new WootNet(optKey, optPort);
  wpad = new WootPad(wnet);

  for (int pos = 1; pos+1 < args.length; pos += 2) wpad.net.addPeer(args[pos], args[pos+1]);


  void doNetworkBusiness () {
    wpad.net.checkTimeouts();

    try {
      wpad.net.processSends();
    } catch (Exception e) {
      conwriteln("NETWORK ERROR: ", e.msg);
    }

    try {
      wpad.net.recv(wpad.text, delegate (in WStrOp op) {
        assert(!op.empty);
        //writeln("REMOTE: ", op);
        wpad.remote(op);
      });
    } catch (Exception e) {
      conwriteln("NETWORK ERROR: ", e.msg);
    }
  }

  version(use_nanovg) {
    // first time setup
    oglSetupDG = delegate () {
      // create and upload texture, rebuild screen
      nvg = createGL2NVG(NVG_FONT_NOAA|NVG_ANTIALIAS|NVG_STENCIL_STROKES/*|NVG_DEBUG*/);
      if (nvg is null) assert(0, "Could not init nanovg.");
      nvg.createFont("sans", optEditorFont);

      glconCtlWindow.addEventListener((NetProcessEvent evt) {
        if (glconCtlWindow !is null && !glconCtlWindow.eventQueued!NetProcessEvent) glconCtlWindow.postTimeout(evNetProcess, 30);
        doNetworkBusiness();
      });
      if (glconCtlWindow !is null && !glconCtlWindow.eventQueued!NetProcessEvent) glconCtlWindow.postTimeout(evNetProcess, 30);
    };


    resizeEventDG = delegate (int wdt, int hgt) {
    };


    // draw main screen
    redrawFrameDG = delegate () {
      // draw main screen
      glClearColor(0, 0, 0, 0);
      glClear(glNVGClearFlags|GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_ACCUM_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
      glViewport(0, 0, glconCtlWindow.width, glconCtlWindow.height);

      if (nvg !is null) {
        nvg.beginFrame(glconCtlWindow.width, glconCtlWindow.height, 1);

        nvg.beginPath();
          //nvg.roundedRect(0, 0, glconCtlWindow.width, glconCtlWindow.height, 3);
          nvg.fillColor(nvgRGB(0, 0, 0));
          nvg.rect(0, 0, glconCtlWindow.width, glconCtlWindow.height);
        nvg.fill();

        nvg.fontSize(optEditorFontSize);
        nvg.fontFace("sans");
        nvg.textAlign(NVGTextAlign.H.Center, NVGTextAlign.V.Middle);
        nvg.fillColor(nvgRGB(255, 255, 0));

        // draw text and cursor
        int curx, cury0, cury1;
        if (wpad.render(nvg, &curx, &cury0, &cury1)) {
          nvg.beginPath();
            nvg.fillColor(nvgRGB(255, 255, 255));
            nvg.rect(curx, cury0, 1, cury1-cury0);
          nvg.fill();
        }

        nvg.endFrame();
      }
    };


    // rebuild main screen (do any calculations we might need)
    nextFrameDG = delegate () {
    };


    keyEventDG = delegate (KeyEvent evt) {
      if (!evt.pressed) return;
      switch (evt.key) {
        case Key.Escape: concmd("quit"); return;
        default:
      }
      wpad.onKeyEvent(evt);
    };

    mouseEventDG = delegate (MouseEvent evt) {
    };

    charEventDG = delegate (dchar ch) {
      //if (ch == 'q') { concmd("quit"); return; }
      wpad.onCharEvent(ch);
    };


    glconRunGLWindowResizeable(WinWidth, WinHeight, "My D App", "SDPY WINDOW");
    if (nvg !is null) { nvg.deleteGL2(); nvg = null; }
  } else {
    sdpyWindowClass = "SDPY WINDOW";
    auto sdwin = new SimpleWindow(WinWidth, WinHeight, "My D App", OpenGlOptions.no, Resizability.fixedSize);

    sdwin.addEventListener((NetProcessEvent evt) {
      if (!sdwin.closed && !sdwin.eventQueued!NetProcessEvent) sdwin.postTimeout(evNetProcess, 30);
      doNetworkBusiness();
      // draw text and cursor
      {
        auto dwg = sdwin.draw();
        int curx, cury0, cury1;
        if (wpad.render(dwg, &curx, &cury0, &cury1)) {
          dwg.fillColor = Color.white;
          dwg.outlineColor = Color.white;
          dwg.drawLine(Point(curx, cury0), Point(curx, cury1));
        }
      }
    });
    sdwin.postTimeout(evNetProcess, 30);

    sdwin.eventLoop(0,
      delegate (KeyEvent evt) {
        if (!evt.pressed) return;
        switch (evt.key) {
          case Key.Escape: sdwin.close(); return;
          default:
        }
        wpad.onKeyEvent(evt);
      },

      delegate (MouseEvent evt) {
      },

      delegate (dchar ch) {
        //if (ch == 'q') { concmd("quit"); return; }
        wpad.onCharEvent(ch);
      },
    );
  }
}
