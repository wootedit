// WOOT network protocol
//
// coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
// Understanding is not required. Only obedience.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// WARNING! crypto here is TOTALLY INVALID! don't do it like that!
module wootnet;

//version = woot_net_chatty;


// ////////////////////////////////////////////////////////////////////////// //
import core.time;

import std.conv : to;
import std.digest.sha;
import std.socket;

import iv.chachasimple;
import iv.cmdcon;
import iv.prng.seeder;
import iv.prng.pcg32;
import iv.unarray;

import wootext;


// ////////////////////////////////////////////////////////////////////////// //
version(Windows) {
  import core.sys.windows.winsock2;
  enum MSG_NOSIGNAL = 0;
} else {
  import core.sys.posix.netinet.in_;
  import core.sys.posix.sys.select;
  import core.sys.posix.sys.socket;
}


// ////////////////////////////////////////////////////////////////////////// //
public string toString() (in auto ref sockaddr_in it) {
  import std.format : format;
  return
    "%u.%u.%u.%u:%u".format(
      it.sin_addr.s_addr&0xff, (it.sin_addr.s_addr>>8)&0xff, (it.sin_addr.s_addr>>16)&0xff, (it.sin_addr.s_addr>>24)&0xff,
      ntohs(it.sin_port));
}


// ////////////////////////////////////////////////////////////////////////// //
align(1) struct WootNetOp {
align(1):
public:
  string toString () const { return op.toString; }

pure nothrow @safe @nogc:
public:
  ubyte[ChaCha20.IVSize] nonce;
  WStrOp op;
  /*
  WStrOp.OpType opType = WStrOp.OpType.None;
  UUID id; // the id assigned at creation-time that this character keeps forever
  uint opid;
  dchar ch; // the user-visible character that this WChar represents; uint.max means "invisible"
  // as per the algorithm outlines in the document, each character specifies which two characters it belongs between
  // these are the ids of the chars that must go somewhere before and somewhere after this character respectively
  UUID prev, next;
  uint popid, nopid;
  */
}


// ////////////////////////////////////////////////////////////////////////// //
// we will use UUIDs to identify clients in the network,

/*
 * WOOT network packet:
 *   uint pktseq;
 *   4 bytes: client id (self for send, remote for recv)
 *   ubyte operation (see PktType)
 *   hello:
 *   insert:
 *     4 bytes: dchar
 *     8 bytes: char id (UUID, opid)
 *     8 bytes: prev id (UUID, opid)
 *     8 bytes: next id (UUID, opid)
 *   delete:
 *     8 bytes: char id (UUID, opid)
 *   ping:
 *     uint pingid
 *   pong:
 *     uint pingid
 *   ack:
 *     pktseq is used as acked packet id
 *   peer:
 *     uint ip
 *     ushort port
 */

class WootNet {
private:
  enum MTU = 1472;

private:
  enum PktType : ubyte {
    Hello,
    Insert,
    Delete,
    Ping,
    Ack,
    Peer,
  }

private:
  static struct Packet {
    uint seq;
    bool ack;
    ubyte[] data;

    @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); return (seq != 0 && data.length >= ChaCha20.IVSize); }

    void clear () { delete data; seq = 0; ack = false; }

    void setup (uint aseqid, bool aack=false) {
      seq = aseqid;
      ack = aack;
      data.unsafeArraySetLength(ChaCha20.IVSize);
      put!uint(aseqid);
    }

    void put(T) (T v) if (__traits(isIntegral, T)) {
      auto dp = cast(const(ubyte)*)&v;
      foreach (ubyte b; dp[0..T.sizeof]) data.unsafeArrayAppend(b);
    }

    /*
    void put(T:UUID) (in auto ref T v) {
      foreach (ubyte b; v.data[]) data.unsafeArrayAppend(b);
    }
    */
  }

  static struct Peer {
    uint cid; // client id [1..]
    Packet[] queue; // send queue
    sockaddr_in addr;
    bool netvalid;
    MonoTime lastActivity;
    bool pingSent;

    void markDead () {
      foreach (ref pkt; queue) pkt.clear();
      delete queue;
      netvalid = false;
    }
  }

  ubyte[ChaCha20.KeySize] mKey;
  PCG32 prng;
  int sk = -1;
  uint pktseq;
  Peer[] mPeers;
  uint[uint] cid2peerMap;
  public uint mMyCId;
  public sockaddr_in myaddr;


  static struct HelloQueue {
    sockaddr_in addr;
    Packet pkt;
  }
  HelloQueue[] mHelloQ;

  int xsend = -1; // <0: hellos; >=0: peers

private:
  Packet buildHello (ref HelloQueue hq, out sockaddr_in aout) {
    if (sk < 0) return Packet();
    if (hq.addr.sin_family != AF_INET) return Packet();
    if (hq.addr.sin_port == 0) return Packet();
    if (hq.addr.sin_addr.s_addr == 0 || hq.addr.sin_addr.s_addr == uint.max) return Packet();
    // send hello packet
    if (!hq.pkt.valid) {
      initPacket(hq.pkt);
      // generate packet
      hq.pkt.put(mMyCId);
      hq.pkt.put(cast(ubyte)PktType.Hello);
      //hq.pkt.put(mPort);
      encodePacket(hq.pkt);
    }
    //return (sendto(sk, hq.pkt.data.ptr, hq.pkt.data.length, MSG_NOSIGNAL, cast(sockaddr*)&hq.addr, hq.addr.sizeof) == hq.pkt.data.length);
    aout = hq.addr;
    return hq.pkt;
  }

  void gotPacketFrom (in ref sockaddr_in addr) {
    foreach (immutable idx, const ref hq; mHelloQ) {
      if (hq.addr.sin_addr.s_addr == addr.sin_addr.s_addr && hq.addr.sin_port == addr.sin_port) {
        mHelloQ.unsafeArrayRemove(cast(int)idx);
        return;
      }
    }
  }

  void advanceXSend () {
    if (xsend < 0) {
      --xsend;
      if (-xsend <= mHelloQ.length) return;
      xsend = 0;
    } else {
      ++xsend;
    }
    for (;;) {
      if (xsend >= mPeers.length) break;
      if (mPeers[xsend].netvalid && mPeers[xsend].queue.length) return;
      ++xsend;
    }
    xsend = -1;
  }

private:
  void encodePacket (ref Packet pkt) {
    if (!pkt.valid) assert(0, "internal error");

    // generate nonce
    foreach (ref ubyte b; pkt.data[0..ChaCha20.IVSize]) {
      b = cast(ubyte)prng.front;
      prng.popFront();
    }

    // encrypt packet
    auto cyph = ChaCha20(mKey, pkt.data[0..ChaCha20.IVSize]);
    cyph.processBuf(pkt.data[ChaCha20.IVSize..$]);
  }

  void initPacket (ref Packet pkt) {
    if (pktseq == 0) ++pktseq;
    pkt.setup(pktseq++);
  }

  Packet encodePacketOp (in ref WootNetOp op) {
    Packet pkt;

    if (op.op.opType == WStrOp.OpType.None || op.op.opType < WStrOp.OpType.min || op.op.opType > WStrOp.OpType.max) return pkt;

    initPacket(pkt);

    // generate packet
    pkt.put(mMyCId);

    final switch (op.op.opType) {
      case WStrOp.OpType.Insert:
        pkt.put(cast(ubyte)PktType.Insert);
        pkt.put(cast(uint)op.op.ch.ch);
        pkt.put(op.op.ch.id.uid);
        pkt.put(op.op.ch.prev.uid);
        pkt.put(op.op.ch.next.uid);
        break;
      case WStrOp.OpType.Delete:
        pkt.put(cast(ubyte)PktType.Delete);
        pkt.put(op.op.ch.id.uid);
        break;
      case WStrOp.OpType.None: assert(0);
    }

    encodePacket(pkt);
    return pkt;
  }

  Packet encodePacketPeer() (in auto ref Peer peer) {
    Packet pkt;
    if (!peer.netvalid) return pkt;

    initPacket(pkt);

    // generate packet
    pkt.put(mMyCId);
    pkt.put(cast(ubyte)PktType.Peer);
    pkt.put(peer.addr.sin_addr.s_addr);
    pkt.put(peer.addr.sin_port);

    //conprintfln("sending peer %s %s", peer.uuid, peer.addr.sin_port);
    encodePacket(pkt);
    return pkt;
  }

  Packet encodePacketAck() (uint seqn) {
    Packet pkt;
    if (seqn == 0) return pkt;

    pkt.setup(seqn, true);

    // generate packet
    pkt.put(mMyCId);
    pkt.put(cast(ubyte)PktType.Ack);

    encodePacket(pkt);
    return pkt;
  }

  Packet encodePacketPing () {
    Packet pkt;

    initPacket(pkt);

    // generate packet
    pkt.put(mMyCId);
    pkt.put(cast(ubyte)PktType.Ping);

    encodePacket(pkt);
    return pkt;
  }

  void peerAck (ref Peer peer, uint seqn) {
    if (!peer.netvalid || seqn == 0 || !peer.netvalid) return;
    // check if we already have this ack
    foreach (immutable idx, ref pkt; peer.queue) {
      if (!pkt.ack) {
        // insert ack
        auto pk = encodePacketAck(seqn);
        if (pk.valid) peer.queue.unsafeArrayInsertBefore(cast(int)idx, pk);
        return;
      }
      if (pkt.seq == seqn) return;
    }
    auto pk = encodePacketAck(seqn);
    if (pk.valid) peer.queue.unsafeArrayInsertBefore(0, pk);
  }

private:
  Peer* newPeer() (uint peercid) {
    if (peercid == 0) return null;
    if (peercid == mMyCId) return null;
    if (auto pip = peercid in cid2peerMap) return &mPeers[*pip];
    mPeers.unsafeArrayAppend(Peer(peercid));
    cid2peerMap[peercid] = cast(uint)(mPeers.length-1);
    mPeers[$-1].lastActivity = MonoTime.currTime;
    return &mPeers[$-1];
  }

  Peer* findPeer() (uint peercid) {
    if (auto pip = peercid in cid2peerMap) return &mPeers[*pip];
    return null;
  }

  Peer* findPeerByAddr() (in auto ref sockaddr_in addr) {
    foreach (immutable idx, ref peer; mPeers) {
      if (!peer.netvalid) continue;
      if (peer.addr.sin_port != addr.sin_port) continue;
      if (peer.addr.sin_addr.s_addr != addr.sin_addr.s_addr) continue;
      return &mPeers[idx];
    }
    return null;
  }

public:
  this (const(char)[] akey, ushort aport) {
    import core.stdc.string : memset;
    memset(&myaddr, 0, myaddr.sizeof);

    // init crypto key
    mKey = sha256Of(akey);
    prng.seed(getUlongSeed, getUlongSeed);

    // add initial peer
    foreach (ref ubyte b; (cast(ubyte*)&mMyCId)[0..mMyCId.sizeof]) {
      b = cast(ubyte)prng.front;
      prng.popFront();
    }

    // prepare socket
    sk = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sk < -1) throw new Exception("cannot create socket");
    myaddr.sin_family = AF_INET;
    myaddr.sin_port = htons(aport);
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(sk, cast(sockaddr*)&myaddr, myaddr.sizeof) == -1) {
      closeSocket();
      throw new Exception("cannot bind socket");
    }
  }

  ~this () { closeSocket(); }

  final private void closeSocket () nothrow @trusted @nogc {
    import core.stdc.string : memset;
    if (sk >= 0) {
      version(Windows) {
      } else {
        import core.sys.posix.unistd : close;
        close(sk);
      }
      sk = -1;
    }
    memset(&myaddr, 0, myaddr.sizeof);
  }

  void addPeer (const(char)[] host, const(char)[] port) {
    import std.socket, std.conv : to;
    auto nport = port.to!ushort;
    auto addr = new InternetAddress(host, nport);
    scope(exit) delete addr;
    addPeer(cast(sockaddr_in)*addr.name);
  }

  void addPeer() (in auto ref sockaddr_in sin) {
    if (sk < 0) return;
    if (sin.sin_port == 0 || sin.sin_addr.s_addr == 0 || sin.sin_addr.s_addr == uint.max) return;
    foreach (const ref hq; mHelloQ) {
      if (hq.addr.sin_addr.s_addr == sin.sin_addr.s_addr && hq.addr.sin_port == sin.sin_port) return; // known peer
    }
    // new peer
    HelloQueue hq;
    hq.addr = sin;
    hq.addr.sin_family = AF_INET;
    mHelloQ.unsafeArrayAppend(hq);
  }

  private final Packet createOpPacket() (in auto ref WStrOp op) {
    if (op.empty) return Packet();
    WootNetOp netop;
    netop.op = op;
    return encodePacketOp(netop);
  }

  final void queue (const(WStrOp)[] ops...) {
    if (mPeers.length == 0) return;
    foreach (const ref op; ops[]) {
      if (op.empty) continue;
      auto pk = createOpPacket(op);
      if (!pk.valid) continue;
      // don't send to ourself
      foreach (ref peer; mPeers[]) {
        if (!peer.netvalid) continue;
        peer.queue.unsafeArrayAppend(pk);
      }
    }
  }

  final void processSends () {
    import core.stdc.string : memset;
    if (sk < 0) return;
    fd_set wrset;
    timeval to;
    foreach (immutable _; 0..mHelloQ.length+mPeers.length) {
      // check if we can send something
      memset(&to, 0, to.sizeof);
      FD_ZERO(&wrset);
      FD_SET(sk, &wrset);
      if (select(sk+1, null, &wrset, null, &to) < 1) return;
      if (!FD_ISSET(sk, &wrset)) return;
      //conwriteln("can send! xsend=", xsend);
      // build packet data
      ubyte[MTU] pkt = 0;
      int pktpos = 0;
      sockaddr_in aout;
      advanceXSend();
      // send something
      if (xsend < 0) {
        if (-xsend <= mHelloQ.length) {
          auto pk = buildHello(mHelloQ[-xsend-1], aout);
          if (pk.valid) {
            pkt[pktpos..pktpos+pk.data.length] = pk.data[];
            pktpos += cast(int)pk.data.length;
          }
        }
      } else if (xsend >= 0 && xsend < mPeers.length && mPeers[xsend].queue.length && mPeers[xsend].netvalid) {
        auto peer = &mPeers[xsend];
        version(woot_net_chatty) conwriteln("sending packet to peer ", peer.cid);
        aout = peer.addr;
        int cnum = 0;
        while (cnum < peer.queue.length) {
          if (pkt.length-pktpos <= peer.queue[cnum].data.length) break;
          pkt[pktpos..pktpos+peer.queue[cnum].data.length] = peer.queue[cnum].data;
          pktpos += cast(int)peer.queue[cnum].data.length;
          // remove ack packets (peers will resend non-acked packets anyway)
          if (peer.queue[cnum].ack) {
            peer.queue[cnum].clear();
            peer.queue.unsafeArrayRemove(cnum);
          } else {
            ++cnum;
          }
        }
      }
      if (pktpos > 0) {
        version(woot_net_chatty) conwriteln("NET: sending packet to ", aout.toString);
        if (sendto(sk, pkt.ptr, pktpos, MSG_NOSIGNAL, cast(sockaddr*)&aout, aout.sizeof) != pktpos) return;
      }
    }
  }

  final void checkTimeouts () {
    auto ct = MonoTime.currTime;
    foreach (ref peer; mPeers[]) {
      if (!peer.netvalid) continue;
      // pinged?
      if (peer.pingSent) {
        if ((ct-peer.lastActivity).total!"msecs" > 1000) {
          // this peer is dead
          conwriteln("NET: disconnected ", peer.cid);
          peer.markDead();
        }
        continue;
      }
      // need to ping?
      if ((ct-peer.lastActivity).total!"msecs" > 500) {
        auto pk = encodePacketPing();
        if (pk.valid) {
          peer.pingSent = true;
          peer.queue.unsafeArrayInsertBefore(0, pk);
        }
        continue;
      }
    }
  }

  final void recv (WString wstr, void delegate (in WStrOp op) cb) {
    import core.stdc.string : memset;

    if (sk < 0) return;
    assert(wstr !is null);
    assert(cb !is null);

    for (;;) {
      fd_set rdset;
      timeval to;
      FD_ZERO(&rdset);
      FD_SET(sk, &rdset);
      if (select(sk+1, &rdset, null, null, &to) < 1) return;
      if (!FD_ISSET(sk, &rdset)) return;

      sockaddr_in it = void;
      socklen_t itlen = it.sizeof;
      memset(&it, 0, it.sizeof);

      // read packet
      ubyte[MTU] pkt = 0;
      auto rd = recvfrom(sk, pkt.ptr, pkt.length, 0/*MSG_DONTWAIT*/, cast(sockaddr*)&it, &itlen);
      if (rd < 0) return;
      gotPacketFrom(it); // clear hellos

      version(woot_net_chatty) conwriteln("NET: packet from ", it.toString);

      int ccpktpos = 0;
      datagram: while (ccpktpos < rd && rd-ccpktpos >= ChaCha20.IVSize) {
        ubyte[MTU] xdata = void;
        xdata[0..rd-ccpktpos] = pkt[ccpktpos..rd];

        // decrypt packet
        auto cyph = ChaCha20(mKey, xdata[0..ChaCha20.IVSize]);
        ccpktpos += ChaCha20.IVSize;
        auto data = xdata[ChaCha20.IVSize..rd];
        cyph.processBuf(data[]);

        if (data.length < 4+4+1) break;

        T get(T) () if (__traits(isIntegral, T)) {
          if (data.length < T.sizeof) throw new Exception("invalid packed received");
          ccpktpos += cast(int)T.sizeof;
          T res = *(cast(T*)data.ptr);
          data = data[T.sizeof..$];
          return res;
        }

        try {
          uint rseq = get!uint;
          uint ruuid = get!uint;
          ubyte opcode = get!ubyte;

          if (opcode < PktType.min || opcode > PktType.max) break; // alas

          auto peer = findPeer(ruuid);
          if (peer is null) {
            // new peer
            version(woot_net_chatty) conwriteln("NET: new peer comes: ", ruuid);
            peer = newPeer(ruuid);
            if (peer is null) break; // oops
            version(woot_net_chatty) conwriteln("  ", peer.cid, " : ", (peer.cid == ruuid));
          }

          version(woot_net_chatty) conprintfln("NET: msg from %u.%u.%u.%u:%u",
            it.sin_addr.s_addr&0xff, (it.sin_addr.s_addr>>8)&0xff, (it.sin_addr.s_addr>>16)&0xff, (it.sin_addr.s_addr>>24)&0xff,
            ntohs(it.sin_port));

          it.sin_family = AF_INET;
          peer.addr = it;
          peer.netvalid = true;
          peer.lastActivity = MonoTime.currTime;
          peer.pingSent = false;

          uint xid;
          switch (opcode) {
            case PktType.Hello:
              // new client arrived
              version(woot_net_chatty) conwriteln("NET: HELLO, port=", htons(peer.addr.sin_port));
              // ack this packet
              peerAck(*peer, rseq);
              // send all known peers to it
              foreach (ref pr; mPeers[]) {
                if (pr.cid != peer.cid) {
                  auto pk = encodePacketPeer(pr);
                  if (pk.valid) peer.queue.unsafeArrayAppend(pk);
                }
              }
              // send all the text to it
              if (wstr !is null) {
                foreach (immutable idx; 0..wstr.length) {
                  auto ch = wstr[idx];
                  if (ch.visible) {
                    auto op = WStrOp(WStrOp.OpType.Insert, wstr[idx]);
                    auto pk = createOpPacket(op);
                    if (pk.valid) peer.queue.unsafeArrayAppend(pk);
                  } else {
                    ch = WChar(ch.id, ' ', ch.prev, ch.next);
                    auto op = WStrOp(WStrOp.OpType.Insert, ch);
                    auto pk = createOpPacket(op);
                    if (pk.valid) peer.queue.unsafeArrayAppend(pk);
                    op = WStrOp(WStrOp.OpType.Delete, wstr[idx]);
                    pk = createOpPacket(op);
                    if (pk.valid) peer.queue.unsafeArrayAppend(pk);
                  }
                }
              }
              break;
            case PktType.Insert:
              version(woot_net_chatty) conwriteln("NET: INSERT");
              // ack this packet
              peerAck(*peer, rseq);
              // char
              dchar ch = cast(dchar)get!uint;
              // char id
              auto chid = WCharId(get!ulong);
              // prev id
              auto previd = WCharId(get!ulong);
              // next id
              auto nextid = WCharId(get!ulong);
              auto wc = WChar(chid, ch, previd, nextid);
              cb(WStrOp(WStrOp.OpType.Insert, wc));
              break;
            case PktType.Delete:
              version(woot_net_chatty) conwriteln("NET: DELETE");
              // ack this packet
              peerAck(*peer, rseq);
              // char id
              auto chid = WCharId(get!ulong);
              auto wc = WChar(chid, 0);
              cb(WStrOp(WStrOp.OpType.Delete, wc));
              break;
            case PktType.Ping:
              // ping request comes
              version(woot_net_chatty) conwriteln("NET: PING");
              // ack this packet
              peerAck(*peer, rseq);
              break;
            case PktType.Ack:
              // ack comes
              version(woot_net_chatty) conwriteln("NET: ACK");
              // check if this is "hello" ack
              foreach (immutable c, ref hq; mHelloQ) {
                if (hq.pkt.valid && hq.pkt.seq == rseq) {
                  version(woot_net_chatty) conwriteln("  hello acked!");
                  hq.pkt.clear();
                  mHelloQ.unsafeArrayRemove(cast(int)c);
                  break;
                }
              }
              // check peer packets
              foreach (immutable c, ref pk; peer.queue) {
                if (pk.seq == rseq) {
                  peer.queue.unsafeArrayRemove(cast(int)c);
                  break;
                }
              }
              break;
            case PktType.Peer:
              // ack this packet
              peerAck(*peer, rseq);
              it.sin_family = AF_INET;
              it.sin_addr.s_addr = get!uint;
              it.sin_port = get!ushort;
              if (it.sin_port == myaddr.sin_port && it.sin_addr.s_addr == myaddr.sin_addr.s_addr) break;
              version(woot_net_chatty) conwriteln("NET: PEER ", it.toString);
              peer = findPeerByAddr(it);
              if (peer is null) addPeer(it);
              break;
            default: throw new Exception("invalid packed received");
          }
        } catch (Exception e) {
          // sorry
          break datagram;
        }
      } // one received datagram
    }
  }

public:
  final @property uint mycid () const pure nothrow @safe @nogc { pragma(inline, true); return mMyCId; }
}


version(Windows) {
shared static this () {
  WSADATA wsa;
  if (WSAStartup(0x0202, &wsa) != 0) assert(0, "cannot initialize shitdoze sockets");
}
}
