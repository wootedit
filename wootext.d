// WithOut Operational Transformation implementation, based on http://hal.inria.fr/inria-00071240/
// partially based on the code by Ryan Kaplan
// Copyright (C) 2011 by Ryan Kaplan
// Partially rewritten by Ketmar // Invisible Vector
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
module wootext /*is aliced*/;

import iv.alice;
import iv.unarray;
import iv.utfutil;


// ////////////////////////////////////////////////////////////////////////// //
// unique ids for chars; cid 0 should not be used (it is special)
struct WCharId {
public:
  string toString() const {
    import std.format : format;
    return "%u/%u".format(cast(uint)(mCOpId>>32), cast(uint)mCOpId);
  }

pure nothrow @safe @nogc:
private:
  ulong mCOpId; // high bits: client id; low bits: char id

public:
  this (uint cid, uint op=0) { pragma(inline, true); mCOpId = ((cast(ulong)cid)<<32)|op; }
  this (ulong aid) { pragma(inline, true); mCOpId = aid; }
  @property ulong uid () const { pragma(inline, true); return mCOpId; }
  @property uint cid () const { pragma(inline, true); return cast(uint)(mCOpId>>32); }
  @property uint opid () const { pragma(inline, true); return cast(uint)mCOpId; }
  bool opEquals() (in WCharId b) const { pragma(inline, true); return (mCOpId == b.mCOpId); }
  int opCmp() (in auto ref WCharId b) const { pragma(inline, true); return (cast(long)(mCOpId-b.mCOpId) > 0)-(cast(long)(mCOpId-b.mCOpId) < 0); }

public:
  alias mCOpId this;
}


// ////////////////////////////////////////////////////////////////////////// //
// this represents a character in our WString
align(1) struct WChar {
align(1):
public:
  string toString () const {
    import std.format : format;
    if (mChar == uint.max) return "WChar(deleted:%s,%s,%s)".format(mId, mPrev, mNext);
    if (mChar <= 32 || mChar >= 127 || mChar == '\'') return "WChar(0x%u,%s,%s,%s)".format(cast(uint)mChar, mId, mPrev, mNext);
    return "WChar('%c',%s,%s,%s)".format(cast(char)mChar, mId, mPrev, mNext);
  }

pure nothrow @safe @nogc:
  static immutable(WChar) begin = WChar(WCharId(0, 0), 0, WCharId(0, 0), WCharId(0, 0));
  static immutable(WChar) end = WChar(WCharId(0, 1), 0, WCharId(0, 1), WCharId(0, 1));
  static immutable(WChar) dummy = WChar(WCharId(0, 0), cast(dchar)uint.max, WCharId(0, 0), WCharId(0, 0));

private:
  WCharId mId; // the id assigned at creation-time that this character keeps forever
  dchar mChar; // the user-visible character that this WChar represents; uint.max means "invisible"
  // as per the algorithm outlines in the document, each character specifies which two characters it belongs between
  // these are the ids of the chars that must go somewhere before and somewhere after this character respectively
  WCharId mPrev, mNext;

public:
  this (in WCharId aid, dchar achar, in WCharId aprev=WCharId(0, 0), in WCharId anext=WCharId(0, 0)) {
    pragma(inline, true);
    mId = aid;
    mChar = achar;
    mPrev = aprev;
    mNext = anext;
  }

  @property auto id () const { pragma(inline, true); return mId; }
  @property dchar ch () const { pragma(inline, true); return mChar; }
  @property auto prev () const { pragma(inline, true); return mPrev; }
  @property auto next () const { pragma(inline, true); return mNext; }
  @property bool visible () const { pragma(inline, true); return (cast(uint)mChar != uint.max); }
  @property void hide () { pragma(inline, true); mChar = cast(dchar)uint.max; }
  @property bool isDummy () const { pragma(inline, true); return (cast(uint)(mId>>32) == 0); }

  bool opEquals() (in auto ref WChar b) const { pragma(inline, true); return (mId == b.mId && mChar == b.mChar && mPrev == b.mPrev && mNext == b.mNext); }
}


// ////////////////////////////////////////////////////////////////////////// //
// WStrOp is generated when a user modifies their copy of a document, and received from other clients to be applied to our WString
align(1) struct WStrOp {
align(1):
public:
  enum OpType { None, Insert, Delete }

public:
  string toString () const {
    import std.format : format;
    return "WStrOp(%s,%s)".format(mOpType, mChar.toString);
  }

pure nothrow @safe @nogc:
private:
  OpType mOpType = OpType.None;
  WChar mChar = WChar.dummy;

public:
  this (OpType aoptype) { pragma(inline, true); mOpType = aoptype; }
  this (OpType aoptype, WChar achar) { pragma(inline, true); mOpType = aoptype; mChar = achar; }

  @property bool empty () const { pragma(inline, true); return (mOpType == OpType.None); }
  @property bool isDel () const { pragma(inline, true); return (mOpType == OpType.Delete); }
  @property bool isIns () const { pragma(inline, true); return (mOpType == OpType.Insert); }
  @property OpType opType () const { pragma(inline, true); return mOpType; }
  @property inout(WChar) ch () inout { pragma(inline, true); return mChar; }
}


// ////////////////////////////////////////////////////////////////////////// //
// this is where most of the collaboration logic lives
class WString {
public:
  alias OnInsertDg = void delegate (int pos, const ref WChar nch);
  alias OnDeleteDg = void delegate (int pos, const ref WChar nch);

public:
  string chars2str () const { import std.format : format; return "%s".format(mChars); }

  override string toString () const nothrow {
    string res;
    char[4] uc = void;
    foreach (const ref ch; mChars[1..$-1]) {
      if (!ch.visible) continue;
      auto len = utf8Encode(uc[], ch.ch);
      res ~= uc[0..len];
    }
    return res;
  }

  final int opApply (scope int delegate (ref WChar ch) dg) const {
    WChar cc;
    foreach (const ref ch; mChars[1..$-1]) {
      cc = ch;
      if (auto res = dg(cc)) return res;
    }
    return 0;
  }

private:
  WChar[] mChars; // list of all WChars that comprise our string
  ulong mNextId; // next charid
  // value: position in `mChars`
  uint[ulong] mCharById;
  bool mPosHashValid;
  // pending operations
  WStrOp[] mOpPool;

public:
  OnInsertDg onInsert;
  OnDeleteDg onDelete;

private:
  void rebuildPosHash () nothrow {
    foreach (immutable idx, const ref ch; mChars[]) mCharById[ch.id] = cast(uint)idx;
    mPosHashValid = true;
  }

public:
  this (uint acid) {
    assert(acid > 0);
    mNextId = (cast(ulong)acid)<<32;
    // put begin marker
    mChars.unsafeArrayAppend(cast(WChar)WChar.begin);
    // put end marker
    mChars.unsafeArrayAppend(cast(WChar)WChar.end);
    // fix position hash
    rebuildPosHash();
  }

  final @property uint cid () const pure nothrow @safe @nogc { pragma(inline, true); return cast(uint)(mNextId>>32); }

  override bool opEquals (in Object o) const nothrow @trusted @nogc {
    if (o is null) return false;
    if (o is this) return true;
    if (auto wc = cast(const(WString))o) return (wc.mChars[] == mChars[]);
    return false;
  }

  // insert `ch` before `position`
  // returns the operation that made the modification
  final WStrOp genInsOp (int position, dchar ch) {
    if (position < 0) position = 0;
    if (position > length) position = length;
    auto nextid = WCharId(mNextId++);
    auto prev = mChars.ptr[position];
    auto next = mChars.ptr[position+1];
    auto newChar = WChar(nextid, ch, prev.id, next.id);
    doIns(newChar);
    return WStrOp(WStrOp.OpType.Insert, newChar);
  }

  // delete char at `position`
  final WStrOp genDelOp (int position) {
    /+
    if (position < 0 || position >= length) return WStrOp();
    auto charToDel = mChars.ptr[position+1]; // +1, to skip `begin` char
    if (doDel(charToDel)) return WStrOp(WStrOp.OpType.Delete, charToDel);
    return WStrOp();
    +/
    if (position >= 0 && position < length && mChars.ptr[position+1].visible) {
      ++position;
      mChars.ptr[position].hide();
      if (onDelete !is null) onDelete(position-1, mChars.ptr[position]);
      return WStrOp(WStrOp.OpType.Delete, mChars.ptr[position]);
    }
    return WStrOp();
  }

  // insert `s` before `position`
  // returns the operation sequence that made the modification
  // WARNING! returned array is not safe for slicing!
  final WStrOp[] genInsOp(T) (uint position, const(T)[] s) if (is(T == char) || is(T == wchar) || is(T == dchar)) {
    WStrOp[] res;
    foreach (immutable ch; s) {
      auto rop = genInsOp(position, ch);
      if (!rop.empty) res.unsafeArrayAppend(rop);
      ++position;
    }
    return res;
  }

  // delete chars at `position`
  // returns the operation sequence that made the modification
  // WARNING! returned array is not safe for slicing!
  final WStrOp[] genDelOp (uint position, int count) {
    WStrOp[] res;
    foreach (immutable _; 0..count) {
      auto rop = genDelOp(position);
      if (!rop.empty) res.unsafeArrayAppend(rop);
      ++position;
    }
    return res;
  }

  // returns `true` if something was changed
  final bool applyRemote(bool doFlush=true) (const(WStrOp)[] ops...) {
    foreach (const ref op; ops[]) if (!op.empty) mOpPool.unsafeArrayAppend(cast(WStrOp)op);
    static if (doFlush) return flushOpPool(); else return false;
  }

  // returns `true` if something was changed
  final bool flushRemoteOps () { pragma(inline, true); return flushOpPool(); }

  // total, including invisible chars (but not marker chars)
  final @property bool empty () const pure nothrow @safe @nogc { pragma(inline, true); return (mChars.length == 2); }
  final @property int length () const pure nothrow @safe @nogc { pragma(inline, true); return cast(int)mChars.length-2; }
  final WChar opIndex (usize pos) const nothrow @trusted @nogc { pragma(inline, true); return (pos < cast(int)mChars.length-2 ? mChars.ptr[pos+1] : WChar.dummy); }

  // char pos by id (suitable for `opIndex()`) or -1
  final int findCharById (const WCharId id) {
    pragma(inline, true);
    auto res = indexOfCharWithId(id);
    return (res > 0 && res < mChars.length-1 ? res+1 : -1);
  }

  // if `[pos]` is invisible, move to next visible
  final int fixVisible (int pos) const nothrow @trusted @nogc {
    if (empty) return 0; // anyway
    if (pos < 0) return firstVisible;
    if (pos >= length) return length;
    while (pos < length && !mChars.ptr[pos+1].visible) ++pos;
    return pos;
  }

  // returned value is always valid
  final int prevVisible (int pos) const nothrow @trusted @nogc {
    if (pos <= 0) return 0;
    if (pos >= length) return lastVisible;
    while (pos > 0 && !mChars.ptr[pos].visible) --pos;
    return (pos ? pos-1 : 0);
  }

  final int nextVisible (int pos) const nothrow @trusted @nogc {
    if (pos < 0) return firstVisible;
    pos += 2;
    while (pos < mChars.length && !mChars.ptr[pos].visible) ++pos;
    if (pos >= mChars.length-1) return length;
    return pos-1;
  }

  // if there are no visible chars, still return 0
  final int firstVisible () const nothrow @trusted @nogc {
    if (empty) return 0;
    int pos = 1;
    while (pos < cast(int)mChars.length-1 && !mChars.ptr[pos].visible) ++pos;
    return (pos < cast(int)mChars.length-1 ? pos+1 : 0);
  }

  // if there are no visible chars, still return `length`
  final int lastVisible () const nothrow @trusted @nogc {
    if (empty) return length;
    int pos = cast(int)mChars.length-2;
    while (pos > 0 && !mChars.ptr[pos].visible) --pos;
    return (pos ? pos-1 : length);
  }

  // ////////////////////////////////////////////////////////////////////// //
  // walking range
  public static struct Range(bool walkFwd, bool onlyVis) {
  nothrow @trusted @nogc:
  private:
    WString str;
    int pos; // current position

  private:
    this (WString astr, int stpos) {
      if (stpos < 0) stpos = 0;
      if (stpos > astr.length) stpos = astr.length;
      str = astr;
      pos = stpos;
      static if (onlyVis) { if (!empty && !front.visible) popFront(); }
    }

  public:
    @property WString text () pure { pragma(inline, true); return str; }
    @property int curpos () const pure { pragma(inline, true); return pos; }
    @property int length () const pure { pragma(inline, true); return (str !is null ? str.length : 0); }
    @property bool empty () const pure { pragma(inline, true); return (str !is null ? (pos < 0 || pos >= str.length) : true); }
    @property WChar front () const { pragma(inline, true); return (!empty ? str.mChars.ptr[pos+1] : WChar.dummy); }
    void popFront () {
      static if (onlyVis) {
        // skip invisible chars
        if (empty) return;
        static if (walkFwd) ++pos; else --pos;
        while (!empty && !str.mChars.ptr[pos+1].visible) {
          static if (walkFwd) ++pos; else --pos;
        }
      } else {
        if (!empty) {
          static if (walkFwd) ++pos; else --pos;
        }
      }
    }
  }

  final auto range(bool walkFwd=true, bool onlyVis=true) (int start=0) nothrow {
    return Range!(walkFwd, onlyVis)(this, start);
  }

private:
  // returns `true` if a character with the passed in id is in this string (visible or not)
  bool contains() (in auto ref WCharId id) const { pragma(inline, true); return ((id.uid in mCharById) !is null); }

  bool isExecutable() (in auto ref WStrOp op) const {
    final switch (op.opType) {
      case WStrOp.OpType.Insert: return (contains(op.ch.prev) && contains(op.ch.next));
      case WStrOp.OpType.Delete: return contains(op.ch.id);
      case WStrOp.OpType.None: return true;
    }
  }

  void doIns() (in auto ref WChar newChar) {
    if (contains(newChar.id)) return; // idempotent
    doInsHelper(newChar, newChar.prev, newChar.next);
  }

  // returns `false` if char was already hidden
  bool doDel() (in auto ref WChar charToDel) {
    if (!mPosHashValid) rebuildPosHash();
    if (auto cpp = charToDel.id in mCharById) {
      if (*cpp > 0 && *cpp < mChars.length-1 && mChars.ptr[*cpp].visible) {
        mChars.ptr[*cpp].hide();
        if (onDelete !is null) onDelete((*cpp)-1, mChars.ptr[*cpp]);
        return true;
      }
    }
    return false;
  }

  // returns position or -1
  int indexOfCharWithId() (in auto ref WCharId cid) {
    if (!mPosHashValid) rebuildPosHash();
    if (auto cpp = cid in mCharById) return *cpp;
    return -1;
  }

  // this function is an iterative version of the logic in the code block at the top of page 11 in the paper.
  void doInsHelper() (in auto ref WChar newChar, WCharId previd, WCharId nextid) {
    for (;;) {
      immutable prevIndex = indexOfCharWithId(previd);
      assert(prevIndex >= 0);

      immutable nextIndex = indexOfCharWithId(nextid);
      assert(nextIndex >= 0);

      assert(nextIndex > prevIndex);

      if (nextIndex == prevIndex+1) {
        // we only have one place for newChar to go, easy deal
        import core.stdc.string : memmove;
        mChars.unsafeArrayAppend(cast(WChar)WChar.end); // arbitrary char
        memmove(mChars.ptr+nextIndex+1, mChars.ptr+nextIndex, (mChars.length-nextIndex-1)*WChar.sizeof);
        mChars.ptr[nextIndex] = newChar;
        mPosHashValid = false;
        mCharById[newChar.id] = 0; // arbitrary value
        if (onInsert !is null) onInsert(nextIndex-1, mChars.ptr[nextIndex]);
        return;
      }

      // see page 11 of the paper; this is non-recursive non-allocating version of that code
      bool placeFound = false;
      int lastGoodIdx = prevIndex; // prevIndex is always good

      foreach (immutable idx; lastGoodIdx+1..nextIndex+1) {
        WChar dc = mChars.ptr[idx];

        auto dcPrevIdx = indexOfCharWithId(dc.prev);
        assert(dcPrevIdx >= 0);

        auto dcNextIdx = indexOfCharWithId(dc.next);
        assert(dcNextIdx >= 0);

        // if this index is good...
        if (dcPrevIdx <= prevIndex && dcNextIdx >= nextIndex) {
          // ...check if we found a boundary
          if (newChar.id < dc.id) {
            // stop right here
            previd = mChars.ptr[lastGoodIdx].id;
            nextid = dc.id;
            placeFound = true;
            break;
          }
          // otherwise, we have new "last good" index
          if (idx != nextIndex) lastGoodIdx = idx;
        }
      }

      // did we found anything?
      if (!placeFound) {
        //writeln("NF: lg=", lastGoodIdx, "; ni=", nextIndex);
        // nope, use `nextIndex` as new next index, lol
        previd = mChars.ptr[lastGoodIdx].id; // always ok
        nextid = mChars.ptr[nextIndex].id;
      }
    }

    assert(0); // the thing that should not be
  }

  // returns the ith visible character in this string; `WChar.begin` and `WChar.end` are both visible
  /+
  WChar ithVisible (uint position) {
    if (!mVisValid) rebuildVisHash();
    if (position >= mVisChars.length) assert(0, "internal error in WootString");
    return mChars.ptr[mVisChars.ptr[position]];
  }
  +/

  bool flushOpPool () {
    bool res = false;
    while (mOpPool.length) {
      bool doAgain = false;
      uint curPos = 0;
      while (curPos < mOpPool.length) {
        if (!isExecutable(mOpPool.ptr[curPos])) { ++curPos; continue; }
        final switch (mOpPool.ptr[curPos].opType) {
          case WStrOp.OpType.Insert: res = true; doIns(mOpPool.ptr[curPos].ch); break;
          case WStrOp.OpType.Delete: res = true; doDel(mOpPool.ptr[curPos].ch); break;
          case WStrOp.OpType.None: break;
        }
        // operation order doesn't matter, so array deletions are easy
        if (curPos < mOpPool.length-1) mOpPool.ptr[curPos] = mOpPool[$-1];
        mOpPool.unsafeArraySetLength(cast(int)mOpPool.length-1);
        doAgain = true;
      }
      if (!doAgain) break;
    }
    return res;
  }
}
